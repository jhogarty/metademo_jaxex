defmodule MetademoJaxexWeb.HostTypeControllerTest do
  use MetademoJaxexWeb.ConnCase

  alias MetademoJaxex.Metademo
  alias MetademoJaxex.Metademo.HostType

  @create_attrs %{
    description: "some description",
    inserted_by: "some inserted_by",
    is_active: 42,
    name: "some name",
    updated_by: "some updated_by"
  }
  @update_attrs %{
    description: "some updated description",
    inserted_by: "some updated inserted_by",
    is_active: 43,
    name: "some updated name",
    updated_by: "some updated updated_by"
  }
  @invalid_attrs %{description: nil, inserted_by: nil, is_active: nil, name: nil, updated_by: nil}

  def fixture(:host_type) do
    {:ok, host_type} = Metademo.create_host_type(@create_attrs)
    host_type
  end

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  describe "index" do
    test "lists all host_types", %{conn: conn} do
      conn = get(conn, Routes.host_type_path(conn, :index))
      assert json_response(conn, 200)["data"] == []
    end
  end

  describe "create host_type" do
    test "renders host_type when data is valid", %{conn: conn} do
      conn = post(conn, Routes.host_type_path(conn, :create), host_type: @create_attrs)
      assert %{"id" => id} = json_response(conn, 201)["data"]

      conn = get(conn, Routes.host_type_path(conn, :show, id))

      assert %{
               "id" => id,
               "description" => "some description",
               "inserted_by" => "some inserted_by",
               "is_active" => 42,
               "name" => "some name",
               "updated_by" => "some updated_by"
             } = json_response(conn, 200)["data"]
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, Routes.host_type_path(conn, :create), host_type: @invalid_attrs)
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "update host_type" do
    setup [:create_host_type]

    test "renders host_type when data is valid", %{
      conn: conn,
      host_type: %HostType{id: id} = host_type
    } do
      conn = put(conn, Routes.host_type_path(conn, :update, host_type), host_type: @update_attrs)
      assert %{"id" => ^id} = json_response(conn, 200)["data"]

      conn = get(conn, Routes.host_type_path(conn, :show, id))

      assert %{
               "id" => id,
               "description" => "some updated description",
               "inserted_by" => "some updated inserted_by",
               "is_active" => 43,
               "name" => "some updated name",
               "updated_by" => "some updated updated_by"
             } = json_response(conn, 200)["data"]
    end

    test "renders errors when data is invalid", %{conn: conn, host_type: host_type} do
      conn = put(conn, Routes.host_type_path(conn, :update, host_type), host_type: @invalid_attrs)
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "delete host_type" do
    setup [:create_host_type]

    test "deletes chosen host_type", %{conn: conn, host_type: host_type} do
      conn = delete(conn, Routes.host_type_path(conn, :delete, host_type))
      assert response(conn, 204)

      assert_error_sent 404, fn ->
        get(conn, Routes.host_type_path(conn, :show, host_type))
      end
    end
  end

  defp create_host_type(_) do
    host_type = fixture(:host_type)
    {:ok, host_type: host_type}
  end
end
