defmodule MetademoJaxexWeb.PodView do
  use MetademoJaxexWeb, :view
  alias MetademoJaxexWeb.PodView

  def render("index.json", %{pod: pod}) do
    %{data: render_many(pod, PodView, "pod.json")}
  end

  def render("show.json", %{pod: pod}) do
    %{data: render_one(pod, PodView, "pod.json")}
  end

  def render("pod.json", %{pod: pod}) do
    %{
      id: pod.id,
      name: pod.name,
      physical_podname: pod.physical_podname,
      type: pod.type,
      db_host: pod.db_host,
      db_host2: pod.db_host2,
      sid: pod.sid,
      port: pod.port,
      zone: pod.zone,
      is_active: pod.is_active,
      inserted_by: pod.inserted_by,
      updated_by: pod.updated_by
    }
  end
end
