# README

This document will cover deploying to Gigalixir.

## GIGALIXIR SETUP


Confirm that you meet or can meet the prerequisites:  https://gigalixir.readthedocs.io/en/latest/main.html#prerequisites

  - python 2.7
  - pip
  - git
  - Linux, OS X or Windows (limited support/beta)


1.  Go to the Gigalixir site:  https://gigalixir.com

2.  Click on the 'Get Started For Free' button (or use direct link: https://www.gigalixir.com/#/signup)

3.  Fill out the form and create your account

4.  Install the command line tool

    ```shell
    sudo pip install gigalixir --ignore-installed six

    # Confirm the tool is working:
    gigalixir --help

    # Example:
    ➜  metademo_jaxex git:(master) ✗ gigalixir --help
    Usage: gigalixir [OPTIONS] COMMAND [ARGS]...

    Options:
    --env TEXT  GIGALIXIR environment [prod, dev].
    -h, --help  Show this message and exit.

    Commands:
    access                          Get permissions for app.
    access:add                      Grants a user permission to deploy an app.
    access:remove                   Denies user access to app.
    account                         Account information.

    {--snip--}

    run                             Run shell command as a job in a separate...
    signup                          Sign up for a new account.
    stack:set                       Set your app stack.
    version                         Show the CLI version.
    ➜  metademo_jaxex git:(master) ✗ 
    ```

5. Log in with the CLI and verify your account

    ```shell
    gigalixir login  # enter your email address and password when prompted

    gigalixir account

    # Example:
    ➜  metademo_jaxex git:(master) ✗ gigalixir account
    {
    "credit_cents": 0, 
    "email": "hogihung@gmail.com", 
    "tier": "FREE"
    }

    ➜  metademo_jaxex git:(master) ✗ 
    ```

6.  Create an app at Gigalixir

  - Using the Command Line Interface (CLI)

    ```shell
    # NOTE: make sure you are in your projects (git) directory
    gigalixir create      # a name will be randomly chosen for you

    -OR-

    gigalixir create -n $APP_NAME    # where $APP_NAME is the name you would like to use
    ```

    -OR-

  - Using the webiste
  - Login to the Gigalixir website using your credentials
  - Navigate to the dashboard if not already there (https://www.gigalixir.com/#/dashboard)
  - Click on the 'Create App' button


7.  Add Gigalixir as a remote git repo

    ```shell
    git remote add gigalixir [YOUR_URL_PROVIDED_TO_YOU_BY_GIGALIXIR_NICE_AND_LONG]
    git push gigalixir master
    ```

8.  Verify your setup

    ```shell
    gigalixir apps

    git remote -v

    # Example:
    ➜  metademo_jaxex git:(master) ✗ gigalixir apps
    [
      {
        "cloud": "gcp", 
        "region": "v2018-us-central1", 
        "replicas": 1, 
        "size": 0.3, 
        "stack": "gigalixir-18", 
        "unique_name": "sympathetic-coordinated-fawn"
      }
    ]

    ➜  metademo_jaxex git:(master) ✗ 

    ➜  metademo_jaxex git:(master) ✗ git remote -v
    gigalixir	https://hogihung%40gmail.com:[API SECRET KEY]@git.gigalixir.com/sympathetic-coordinated-fawn.git (fetch)
    gigalixir	https://hogihung%40gmail.com:[API SECRET KEY]@git.gigalixir.com/sympathetic-coordinated-fawn.git (push)
    origin	git@gitlab.com:jhogarty/metademo_jaxex.git (fetch)
    origin	git@gitlab.com:jhogarty/metademo_jaxex.git (push)
    ➜  metademo_jaxex git:(master) ✗ 
    ```

9. Manually deploy the app

    ```shell
    git push gigalixir master

    # Check app (you may need to wait a few seconds)
    gigalixir ps

    # Example:
    ➜  metademo_jaxex git:(master) ✗ gigalixir ps
    {
    "cloud": "gcp", 
    "pods": [
        {
        "lastState": {}, 
        "name": "sympathetic-coordinated-fawn-57677fd85-5bswt", 
        "status": "Healthy", 
        "version": "4"
        }
    ], 
    "region": "v2018-us-central1", 
    "replicas_desired": 1, 
    "replicas_running": 1, 
    "size": 0.3, 
    "stack": "gigalixir-18", 
    "unique_name": "sympathetic-coordinated-fawn"
    }

    ➜  metademo_jaxex git:(master) ✗ 

    # NOTE: your output will be similar, but not an exact match.
    ```

10. Create (free) Postgres DB instance

    ```shell
    gigalixir pg:create --free

    # Check status
    gigalixir pg

    # Example:
    ➜  metademo_jaxex git:(master) ✗ gigalixir pg              
    [
    {
        "app_name": "sympathetic-coordinated-fawn", 
        "database": "d4a00b51-0786-4323-9f95-9719015da221", 
        "host": "postgres-free-tier-1.gigalixir.com", 
        "id": "d4a00b51-0786-4323-9f95-9719015da221", 
        "limited_at": null, 
        "password": "[YOUR_PASSWORD_HERE]", 
        "port": 5432, 
        "state": "AVAILABLE", 
        "tier": "FREE", 
        "url": "postgresql://[YOUR_INFO_HERE]@postgres-free-tier-1.gigalixir.com:5432/[YOUR_INFO_HERE]", 
        "username": "[YOUR_USER_NAME_HERE]"
    }
    ]

    ➜  metademo_jaxex git:(master) ✗ 
    # NOTE:  Most of the above values will be different for your instance.
    ```

11. Review the config

    ```shell
    ➜  metademo_jaxex git:(master) ✗ gigalixir config
    {
    "DATABASE_URL": "ecto://[YOUR_INFO_HERE]a@postgres-free-tier-1.gigalixir.com:5432/[MORE_OF_YOUR_INFO_HERE]"
    }

    ➜  metademo_jaxex git:(master) ✗ 
    ```

12. Migrations

    ```shell
    ➜  metademo_jaxex git:(master) gigalixir run mix ecto.migrate
    Starting new container to run: `mix ecto.migrate`.
    See `gigalixir logs` for any output.
    See `gigalixir ps` for job info.
    ➜  metademo_jaxex git:(master)

    # Note: use the gigalixir logs command to review if migrations ran:
    2019-10-09T02:11:14.838717+00:00 sympathetic-coordinated-fawn[gigalixir-run]: Attempting to run 'mix ecto.migrate' in a new container.
    2019-10-09T02:11:17.984476+00:00 sympathetic-coordinated-fawn[sympathetic-coordinated-fawn-run-p4tdh]: 
    2019-10-09T02:11:18.066597+00:00 sympathetic-coordinated-fawn[sympathetic-coordinated-fawn-run-p4tdh]: 
    2019-10-09T02:11:17.984491+00:00 sympathetic-coordinated-fawn[sympathetic-coordinated-fawn-run-p4tdh]: 02:11:17.984 [info]  == Running 20190902194616 MetademoJaxex.Repo.Migrations.CreateHostRoles.change/0 forward
    2019-10-09T02:11:17.984822+00:00 sympathetic-coordinated-fawn[sympathetic-coordinated-fawn-run-p4tdh]: 02:11:17.984 [info]  create table host_roles
    2019-10-09T02:11:17.984821+00:00 sympathetic-coordinated-fawn[sympathetic-coordinated-fawn-run-p4tdh]: 
    2019-10-09T02:11:18.066869+00:00 sympathetic-coordinated-fawn[sympathetic-coordinated-fawn-run-p4tdh]: 02:11:18.066 [info]  create index host_roles_name_index

    {--snip--}
    ```

13. Seed your app with data (optional)

    ```shell
    ➜  metademo_jaxex git:(master) gigalixir run mix run priv/repo/seeds.exs
    Starting new container to run: `mix run priv/repo/seeds.exs`.
    See `gigalixir logs` for any output.
    See `gigalixir ps` for job info.
    ➜  metademo_jaxex git:(master)
    ```

14. ssh/remote access

    In order to run commands on the remote gigalixir instance, you will need to
    add your ssh keys.  Change the path and file name in the example below if
    needed to match where your key is stored:

    ```shell
    gigalixir account:ssh_keys:add "$(cat ~/.ssh/id_rsa.pub)"

    # Example:
    ➜  metademo_jaxex git:(master) ✗ gigalixir account:ssh_keys:add "$(cat ~/.ssh/id_rsa.pub)"
    Please allow a few minutes for the SSH key to propagate to your run containers.
    ➜  metademo_jaxex git:(master) ✗ 
    ```
    
    Now you can connect to your remote instance.  For example:


    ```shell
    ➜  metademo_jaxex git:(deploy_gigalixir) ✗ gigalixir ps:remote_console
    Erlang/OTP 21 [erts-10.0] [source] [64-bit] [smp:4:4] [ds:4:4:10] [async-threads:1] [hipe]

    Interactive Elixir (1.7.4) - press Ctrl+C to exit (type h() ENTER for help)
    iex(sympathetic-coordinated-fawn@10.56.13.78)1> 
    ```

15. Check the logs of your gigalixir instance

    ```shell
    ➜  metademo_jaxex git:(master) ✗ gigalixir logs             
    {"errors":{"":["Looks like you haven't deployed yet."]}}
    Exception while posting item ConnectionError(MaxRetryError("HTTPSConnectionPool(host='api.rollbar.com', port=443): Max retries exceeded with url: /api/1/item/ (Caused by NewConnectionError('<requests.packages.urllib3.connection.VerifiedHTTPSConnection object at 0x7f3c44d9d650>: Failed to establish a new connection: [Errno 111] Connection refused',))",),)
    Traceback (most recent call last):
      File "/home/jfhogarty/.local/lib/python2.7/site-packages/rollbar/__init__.py", line 1263, in _send_payload
        _post_api('item/', payload_str, access_token=access_token)

    {--snip--}

    ```

[ElixirCasts - Deploying to Gigalixir](https://elixircasts.io/deploying-with-gigalixir)


