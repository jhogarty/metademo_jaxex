defmodule MetademoJaxexWeb.PodControllerTest do
  use MetademoJaxexWeb.ConnCase

  alias MetademoJaxex.Metademo
  #  alias MetademoJaxex.Metademo.Pod

  @create_attrs %{
    db_host: "some db_host",
    db_host2: "some db_host2",
    inserted_by: "some inserted_by",
    is_active: 42,
    name: "some name",
    physical_podname: "some physical_podname",
    port: "some port",
    sid: "some sid",
    type: "some type",
    updated_by: "some updated_by",
    zone: "some zone"
  }
  # @update_attrs %{
  #   db_host: "some updated db_host",
  #   db_host2: "some updated db_host2",
  #   inserted_by: "some updated inserted_by",
  #   is_active: 43,
  #   name: "some updated name",
  #   physical_podname: "some updated physical_podname",
  #   port: "some updated port",
  #   sid: "some updated sid",
  #   type: "some updated type",
  #   updated_by: "some updated updated_by",
  #   zone: "some updated zone"
  # }
  # @invalid_attrs %{db_host: nil, db_host2: nil, inserted_by: nil, is_active: nil, name: nil, physical_podname: nil, port: nil, sid: nil, type: nil, updated_by: nil, zone: nil}

  def fixture(:pod) do
    {:ok, pod} = Metademo.create_pod(@create_attrs)
    pod
  end

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  describe "index" do
    test "lists all pod", %{conn: conn} do
      conn = get(conn, Routes.pod_path(conn, :index))
      assert json_response(conn, 200)["data"] == []
    end
  end

  # TODO: FIXME 09/11/2019 - HOGI
  # describe "create pod" do
  #   test "renders pod when data is valid", %{conn: conn} do
  #     conn = post(conn, Routes.pod_path(conn, :create), pod: @create_attrs)
  #     assert %{"id" => id} = json_response(conn, 201)["data"]

  #     conn = get(conn, Routes.pod_path(conn, :show, id))

  #     assert %{
  #              "id" => id,
  #              "db_host" => "some db_host",
  #              "db_host2" => "some db_host2",
  #              "inserted_by" => "some inserted_by",
  #              "is_active" => 42,
  #              "name" => "some name",
  #              "physical_podname" => "some physical_podname",
  #              "port" => "some port",
  #              "sid" => "some sid",
  #              "type" => "some type",
  #              "updated_by" => "some updated_by",
  #              "zone" => "some zone"
  #            } = json_response(conn, 200)["data"]
  #   end

  #   test "renders errors when data is invalid", %{conn: conn} do
  #     conn = post(conn, Routes.pod_path(conn, :create), pod: @invalid_attrs)
  #     assert json_response(conn, 422)["errors"] != %{}
  #   end
  # end

  # describe "update pod" do
  #   setup [:create_pod]

  #   test "renders pod when data is valid", %{conn: conn, pod: %Pod{id: id} = pod} do
  #     conn = put(conn, Routes.pod_path(conn, :update, pod), pod: @update_attrs)
  #     assert %{"id" => ^id} = json_response(conn, 200)["data"]

  #     conn = get(conn, Routes.pod_path(conn, :show, id))

  #     assert %{
  #              "id" => id,
  #              "db_host" => "some updated db_host",
  #              "db_host2" => "some updated db_host2",
  #              "inserted_by" => "some updated inserted_by",
  #              "is_active" => 43,
  #              "name" => "some updated name",
  #              "physical_podname" => "some updated physical_podname",
  #              "port" => "some updated port",
  #              "sid" => "some updated sid",
  #              "type" => "some updated type",
  #              "updated_by" => "some updated updated_by",
  #              "zone" => "some updated zone"
  #            } = json_response(conn, 200)["data"]
  #   end

  #   test "renders errors when data is invalid", %{conn: conn, pod: pod} do
  #     conn = put(conn, Routes.pod_path(conn, :update, pod), pod: @invalid_attrs)
  #     assert json_response(conn, 422)["errors"] != %{}
  #   end
  # end

  # describe "delete pod" do
  #   setup [:create_pod]

  #   test "deletes chosen pod", %{conn: conn, pod: pod} do
  #     conn = delete(conn, Routes.pod_path(conn, :delete, pod))
  #     assert response(conn, 204)

  #     assert_error_sent 404, fn ->
  #       get(conn, Routes.pod_path(conn, :show, pod))
  #     end
  #   end
  # end

  # defp create_pod(_) do
  #   pod = fixture(:pod)
  #   {:ok, pod: pod}
  # end
end
