# README

The goal of this app is to demonstrate how one can leverage the power of GitLab for their Elixir project and take advantage of GitLab's CI/CD. This repo contains the following branches which one can review to see different approaches for CI and CD:

1. [master](https://gitlab.com/jhogarty/metademo_jaxex/tree/master)           - main branch that has our demo app code (deploys to Gigalixir)
2. [testing_ci](https://gitlab.com/jhogarty/metademo_jaxex/tree/testing_ci)       - branch with .gitlab-ci.yml that will run test suite with parallel testing jobs
3. [deploy_gigalixir](https://gitlab.com/jhogarty/metademo_jaxex/tree/deploy_gigalixir) - branch that has full/complex .gitlab-ci.yml file, including deploying
4. [deploy_heroku](https://gitlab.com/jhogarty/metademo_jaxex/tree/deploy_heroku)    - branch with full/complex .gitlab-ci.yml, including deploying
5. [trial_docker_image_cicd](https://gitlab.com/jhogarty/metademo_jaxex/tree/trial_docker_image_cicd) - branch that illustrates building an elixir image, storing it in GitLab's Container Registry, for use in your CI/CD
6. [using_docker](https://gitlab.com/jhogarty/metademo_jaxex/tree/using_docker)     - branch with a Dockerfile and docker-compose.yml file for use with spinning up your app using Docker Compose

**PREREQUISITES:**

- Elixir 1.7.4
- Phoenix 1.4.9

This project will be built so that it can run in Docker containers.  The application will run in at least one container and the database will run in its' own conatiner.

With that in mind, though not a hard requirement, it is strongly suggested that you have Docker working in your development environment and run the database in one container and this application in another (see docs/how\_to\_docker\_compose.md.)

## GITLAB SIGNUP

1. Go to GitLab's site:  https://about.gitlab.com

2. Click on the link for Register (or use direct link: https://gitlab.com/users/sign_in#register-pane)

3. Fill out the form on the Register tab to create your account

## DOCUMENTS

- [docs/how\_to\_gigalixir.md](../blob/using_docker/docs/how_to_gigalixir.md)
- [docs/how\_to\_heroku.md](../blob/using_docker/docs/how_to_heroku.md)
- [docs/how\_to\_iex.md](../blob/using_docker/docs/how_to_iex.md)
- [docs/how\_to_docker\_compose.md](../blob/using_docker/docs/how_to_docker_compose.md)
- [docs/app\_creation\_commands\_and\_examples.md](../blob/using_docker/docs/app_creation_commands_and_examples.md)

## MetademoJaxex

This demo application is loosely built upon a Ruby (Grape) application that I use in my day job.  Things have been slimmed down and simplified.  There are tests that are currently commented out since I used the Phoenix generators to get things up and running quickly.  Feel free to challenge yourself and get those tests working.  I do plan on fixing them in the future as well and add more examples, e.g. deploying to Digital Ocean using GitLab CI/CD.

Let's take a quick look at some of the data components of this app:

### Host Role

- attribute of a host via relationship

### Host Type

- attribute of a host via relationship

### Datacenter

- attribute of a host via relationship
- attribute of a pod via relationship

### Host

- composed of some attributes
- belongs to a datacenter
- belongs to a host role
- belongs to a host type
- has zero to many host_attrs
- may belong to a Pod

### Pod
  
- composed of some attributes
- belongs to a datacenter
- has zero to many hosts through pod_members

NOTE: a big thank you to Mr. Micah Cooper for assisting me with resolving the Ecto Association bits for Pod Members.

For this demo app, I utilized the built-in Phoenix generator:

```shell
                  CONTEXT  SCHEMA   TABLE_NAME COLUMN_NAME:TYPE....
 mix phx.gen.json Metademo HostRole host_roles name:string description:string is_active:integer inserted_by:string updated_by:string
 mix phx.gen.json Metademo HostType host_types name:string description:string is_active:integer inserted_by:string updated_by:string
 mix phx.gen.json Metademo Datacenter datacenters code:string name:string pattern:string inserted_by:string updated_by:string is_active:integer

 mix phx.gen.json Metademo Host hosts hostname:string is_active:integer host_role_id:references:host_roles host_type_id:references:host_types datacenter_id:references:datacenters inserted_by:string updated_by:string uuid:string ip_address:string

 mix phx.gen.json Metademo HostAttr host_attrs name:string value:string is_active:integer host_id:references:hosts inserted_by:string updated_by:string

 mix phx.gen.json Metademo Pod pod name:string physical_podname:string type:string db_host:string db_host2:string sid:string port:string zone:string is_active:integer inserted_by:string updated_by:string

 mix phx.gen.json Metademo PodMember pod_members pod_id:integer host_id:integer is_active:integer inserted_by:string updated_by:string
```

## LINKS

### GitLab Links

- [GitLab CI/CD](https://about.gitlab.com/product/continuous-integration/)
- [Introducing GitLab CI/CD (Phoenix app)](https://docs.gitlab.com/ee/ci/examples/test_phoenix_app_with_gitlab_ci_cd/)
- [Testing a Phoenix application with GitLab CI/CD](https://docs.gitlab.com/ce/ci/examples/test_phoenix_app_with_gitlab_ci_cd/index.html)
- [GitLab Pipelines](https://docs.gitlab.com/ee/ci/pipelines.html)
- [GitLab Pipeline Configuration](https://docs.gitlab.com/ee/ci/yaml/README.html)
- [GitLab CI/CD Environment Variable](https://docs.gitlab.com/ee/ci/variables/README.html#gitlab-ciyml-defined-variables)
- [gitlab-ci.yml](https://docs.gitlab.com/ee/ci/yaml/)

### Metademo Jaxex Demo App (Gigalixir)

- [Gigalixir (admin) Demo App - JaxEx](https://www.gigalixir.com/#/apps/sympathetic-coordinated-fawn)  
- [Published Demo App - JaxEx](http://sympathetic-coordinated-fawn.gigalixirapp.com/api/hosts)

## Gigalixir Related Links

- [What is Gigalixir](https://gigalixir.readthedocs.io/en/latest/main.html#screencast)
- [Continuous deployment of a Phoenix project using GitLab CI/CD](https://blog.kewah.com/2019/gitlab-ci-for-elixir-phoenix-project/)
- [Setting up automated testing and deployment to Gigalixir for a Phoenix Application using GitLab](https://dev.to/edwinthinks/setting-up-automated-testing-and-deployment-to-gigalixir-for-a-phoenix-application-using-gitlab-3oe5)
- [Deploying to Gigalixir](https://elixircasts.io/deploying-with-gigalixir)

### Miscellaneous Links

- [Testing your Phoenix + Elixir + Postgres app with GitLab CI](https://nickvernij.nl/blog/github-ci-elixir.html)
- [Linting Elixir App - Credo](https://github.com/rrrene/credo)
- [Mix Format](https://hexdocs.pm/mix/master/Mix.Tasks.Format.html)
- [GitHub getting CI (beta)](https://techcrunch.com/2019/08/08/github-actions-is-now-a-ci-cd-service/)
  
### Heroku Related Links

- [GitLab CI/CD with Heroku](https://medium.com/swlh/how-do-i-deploy-my-code-to-heroku-using-gitlab-ci-cd-6a232b6be2e4)
- [Deploying to Heroku](https://elixircasts.io/deploying-elixir-with-heroku)

