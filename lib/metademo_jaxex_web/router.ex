defmodule MetademoJaxexWeb.Router do
  use MetademoJaxexWeb, :router

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/api", MetademoJaxexWeb do
    pipe_through :api

    resources "/host_roles", HostRoleController, except: [:new, :edit]
    resources "/host_types", HostTypeController, except: [:new, :edit]
    resources "/datacenters", DatacenterController, except: [:new, :edit]
    resources "/hosts", HostController, except: [:new, :edit]
    resources "/host_attrs", HostAttrController, except: [:new, :edit]
    resources "/pods", PodController, except: [:new, :edit]
  end
end
