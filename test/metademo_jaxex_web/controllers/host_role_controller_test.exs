defmodule MetademoJaxexWeb.HostRoleControllerTest do
  use MetademoJaxexWeb.ConnCase

  alias MetademoJaxex.Metademo
  alias MetademoJaxex.Metademo.HostRole

  @create_attrs %{
    description: "some description",
    inserted_by: "some inserted_by",
    is_active: 42,
    name: "some name",
    updated_by: "some updated_by"
  }
  @update_attrs %{
    description: "some updated description",
    inserted_by: "some updated inserted_by",
    is_active: 43,
    name: "some updated name",
    updated_by: "some updated updated_by"
  }
  @invalid_attrs %{description: nil, inserted_by: nil, is_active: nil, name: nil, updated_by: nil}

  def fixture(:host_role) do
    {:ok, host_role} = Metademo.create_host_role(@create_attrs)
    host_role
  end

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  describe "index" do
    test "lists all host_roles", %{conn: conn} do
      conn = get(conn, Routes.host_role_path(conn, :index))
      assert json_response(conn, 200)["data"] == []
    end
  end

  describe "create host_role" do
    test "renders host_role when data is valid", %{conn: conn} do
      conn = post(conn, Routes.host_role_path(conn, :create), host_role: @create_attrs)
      assert %{"id" => id} = json_response(conn, 201)["data"]

      conn = get(conn, Routes.host_role_path(conn, :show, id))

      assert %{
               "id" => id,
               "description" => "some description",
               "inserted_by" => "some inserted_by",
               "is_active" => 42,
               "name" => "some name",
               "updated_by" => "some updated_by"
             } = json_response(conn, 200)["data"]
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, Routes.host_role_path(conn, :create), host_role: @invalid_attrs)
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "update host_role" do
    setup [:create_host_role]

    test "renders host_role when data is valid", %{
      conn: conn,
      host_role: %HostRole{id: id} = host_role
    } do
      conn = put(conn, Routes.host_role_path(conn, :update, host_role), host_role: @update_attrs)
      assert %{"id" => ^id} = json_response(conn, 200)["data"]

      conn = get(conn, Routes.host_role_path(conn, :show, id))

      assert %{
               "id" => id,
               "description" => "some updated description",
               "inserted_by" => "some updated inserted_by",
               "is_active" => 43,
               "name" => "some updated name",
               "updated_by" => "some updated updated_by"
             } = json_response(conn, 200)["data"]
    end

    test "renders errors when data is invalid", %{conn: conn, host_role: host_role} do
      conn = put(conn, Routes.host_role_path(conn, :update, host_role), host_role: @invalid_attrs)
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "delete host_role" do
    setup [:create_host_role]

    test "deletes chosen host_role", %{conn: conn, host_role: host_role} do
      conn = delete(conn, Routes.host_role_path(conn, :delete, host_role))
      assert response(conn, 204)

      assert_error_sent 404, fn ->
        get(conn, Routes.host_role_path(conn, :show, host_role))
      end
    end
  end

  defp create_host_role(_) do
    host_role = fixture(:host_role)
    {:ok, host_role: host_role}
  end
end
