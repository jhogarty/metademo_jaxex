defmodule MetademoJaxexWeb.HostRoleController do
  use MetademoJaxexWeb, :controller

  alias MetademoJaxex.Metademo
  alias MetademoJaxex.Metademo.HostRole

  action_fallback MetademoJaxexWeb.FallbackController

  def index(conn, _params) do
    host_roles = Metademo.list_host_roles()
    render(conn, "index.json", host_roles: host_roles)
  end

  def create(conn, %{"host_role" => host_role_params}) do
    with {:ok, %HostRole{} = host_role} <- Metademo.create_host_role(host_role_params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", Routes.host_role_path(conn, :show, host_role))
      |> render("show.json", host_role: host_role)
    end
  end

  def show(conn, %{"id" => id}) do
    host_role = Metademo.get_host_role!(id)
    render(conn, "show.json", host_role: host_role)
  end

  def update(conn, %{"id" => id, "host_role" => host_role_params}) do
    host_role = Metademo.get_host_role!(id)

    with {:ok, %HostRole{} = host_role} <- Metademo.update_host_role(host_role, host_role_params) do
      render(conn, "show.json", host_role: host_role)
    end
  end

  def delete(conn, %{"id" => id}) do
    host_role = Metademo.get_host_role!(id)

    with {:ok, %HostRole{}} <- Metademo.delete_host_role(host_role) do
      send_resp(conn, :no_content, "")
    end
  end
end
