defmodule MetademoJaxex.Repo.Migrations.CreatePod do
  use Ecto.Migration

  def change do
    create table(:pods) do
      add :name, :string, null: false
      add :physical_podname, :string
      add :db_host, :string
      add :db_host2, :string
      add :port, :string
      add :sid, :string
      add :type, :string, null: false
      add :zone, :string, null: false
      add :is_active, :integer, null: false, default: 1
      add :inserted_by, :string, null: false
      add :updated_by, :string, null: false
      add :datacenter_id, references(:datacenters, on_delete: :nothing), null: false

      timestamps()
    end

    create index(:pods, [:datacenter_id])
    create unique_index(:pods, [:name])
  end
end
